﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RTS_Option
{
    public string text;
    public RTS_ICoversation next;
}
