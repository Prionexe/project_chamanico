﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RTS_Sentence
{
    public RTS_Actor actor;
    public string text;
    public RTS_Actor.Expression expression;
}

