﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RTS_CityMenuManager : MonoBehaviour
{
    public List<GameObject> Buttons;

    public void Start()
    {
        /*foreach(GameObject button in Buttons)
        {
            button.SetActive(false);
        }

        switch(RTS_GameManager.Instance.stage)
        {
            case 1:
                Buttons[0].SetActive(true);
                Buttons[1].SetActive(true);
                Buttons[5].SetActive(true);
                break;
            case 2:
                Buttons[0].SetActive(true);
                Buttons[1].SetActive(true);
                Buttons[5].SetActive(true);
                Buttons[3].SetActive(true);
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            default:
                foreach (GameObject b in Buttons)
                    b.SetActive(true);
                ;break;
        }*/

    }

    public void OpenDialogScene(int i)
    {
        RTS_GameManager.Instance.SetDialog(i);
        RTS_GameManager.Instance.LoadScene("RTS_Dialog");
    }

    public void SetPerson(int i)
    {
        RTS_GameManager.Instance.person = i;
    }

    public void SetDungeon(int i)
    {
        RTS_GameManager.Instance.SetDungeon(i);
    }

}
