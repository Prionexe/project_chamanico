﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RTS_ZoneMapManager : MonoBehaviour {

    public Button city;
    public Button mountain;
    public Button lake;
    public Button forest;
    public Button vulcano;
    int actives;
    public BooleanValue value;


    // Use this for initialization
    void Start ()
    {
        actives = 0;
        if (RTS_GameManager.Instance.dungeons[1].cleared)
        {
            forest.gameObject.SetActive(false);
            actives++;
        }
        if (RTS_GameManager.Instance.dungeons[2].cleared)
        {
            mountain.gameObject.SetActive(false);
            actives++;
        }
        if (RTS_GameManager.Instance.dungeons[3].cleared)
        {
            lake.gameObject.SetActive(false);
            actives++;
        }
        if (RTS_GameManager.Instance.dungeons[4].cleared)
        {
            vulcano.gameObject.SetActive(false);
            actives++;
        }
        if (actives < 4) city.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SelectDungeon(int i)
    {
        RTS_GameManager.Instance.SetDungeon(i);
    }

    public void SelectDialog(int i)
    {
        RTS_GameManager.Instance.SetDialog(i);
        RTS_GameManager.Instance.LoadScene("RTS_Dialog");
    }

    public void StartDungeon(int i)
    {
        Debug.Log("A");
        Debug.Log(RTS_GameManager.Instance);
        RTS_GameManager.Instance.EnterDungeon(i);
    }

    public void SelectFinalDungeon()
    {
        if(value.value)
            RTS_GameManager.Instance.SetDungeon(RTS_GameManager.Instance.dungeons.Count - 2);
        else
            RTS_GameManager.Instance.SetDungeon(RTS_GameManager.Instance.dungeons.Count - 1);
    }
}
