﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RTS_MainMenuManager : MonoBehaviour {

    public GameObject panelNewPlayer;
    public GameObject panelYouSureNewGame;
    public GameObject panelYouSureExit;
    public GameObject panelOptions;

    public Text newName;

    public GameObject continueButton;

    private void Awake()
    {
        Init();
    }

    // Use this for initialization
    void Start()
    {
    }

    public void Init()
    {
        panelNewPlayer.SetActive(false);
        panelYouSureNewGame.SetActive(false);
        panelYouSureExit.SetActive(false);
        panelOptions.SetActive(false);
    }

    public void NewGameCheck()
    {
        Debug.Log("nuevo juego");
        // ver si no tengo otra partida creada
        if (RTS_GameManager.Instance.playerData.stage > 0)
        {
            panelYouSureNewGame.SetActive(true);
        }
        else
        {
            panelNewPlayer.SetActive(true);
        }
    }

    public void NewPlayer()
    {
        Debug.Log(newName.text);
        RTS_GameManager.Instance.NewPlayer(newName.text);
    }

    public void StartGame()
    {
        RTS_GameManager.Instance.NewGame();
        //SceneManager.LoadScene("RTS_Dialog");
    }

    public void CloseGame()
    {
        RTS_GameManager.Instance.CloseGame();
    }

    public void ContinueGame()
    {
        Debug.Log("Continuando");
    }
}
