﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RTS_TalkManager : MonoBehaviour
{
    public string endTo = "RTS_CityMap";

    public RTS_ICoversation actual;

    public Image scenario;

    public Image actorImage;
    public Text actorName;
    public Text showingText;

    //options
    public Text headerText;
    public List<Button> buttons;
    public List<Text> textButtons;

    public bool overtake = false;
    public bool next = false;

    public GameObject OptionPanel;
    public GameObject TalkPanel;

    public bool nextConversation = false;

    public void Start()
    {
        OptionPanel.SetActive(false);
        TalkPanel.SetActive(false);
        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(false);
        }
        actual = RTS_GameManager.Instance.historyFlow.actual;
        Show(actual);
    }

    public void Update()
    {
        if (Input.GetButtonUp("Fire1"))
        {
            if (overtake)
            {
                next = true;
            }
            else
            {
                overtake = true;
            }
        }

        if (nextConversation)
        {
            if (actual == null)
            {
                SceneManager.LoadScene(endTo);
                Debug.Log(endTo);
                return;
            }

            nextConversation = false;
            OptionPanel.SetActive(false);
            TalkPanel.SetActive(false);
            foreach (Button b in buttons)
            {
                b.gameObject.SetActive(false);
            }
            Show(actual);
        }
    }


    public void SetActual(RTS_ICoversation i)
    {
        actual = i;
    }

    public void Show(RTS_ICoversation conversation)
    {
        

        switch (conversation.GetTypeCoversation())
        {
            case RTS_TypeConversation.DIALOG:
                TalkPanel.SetActive(true);
                StartCoroutine(ShowDialog(conversation));
                break;

            case RTS_TypeConversation.OPTIONS:
                OptionPanel.SetActive(true);
                StartCoroutine(ShowOptions(conversation));
                break;
            case RTS_TypeConversation.END:
                Debug.Log("fin");
                ((RTS_DialogEnd)conversation).GoTo();
                nextConversation = true;
                break;
            case RTS_TypeConversation.CONDITION:
                Condition(conversation);
                break;
            case RTS_TypeConversation.CHOOSE:
                Choose(conversation);
                break;
        }

    }

    private IEnumerator ShowOptions(RTS_ICoversation conversation)
    {
        RTS_Decision desition = (RTS_Decision)conversation;
        scenario.sprite = desition.scenario;
        actorImage.sprite = desition.actor.expreccions[0];

        int i = 0;
        next = false;
        headerText.text = desition.headerText;
        foreach (RTS_Option opt in desition.options)
        {
            Debug.Log(i);
            textButtons[i].text = opt.text;
            buttons[i].onClick.AddListener(delegate { SetActual(opt.next); SetNext(true); });
            buttons[i].gameObject.SetActive(true);
            i++;
        }
        yield return new WaitUntil(() => next);
        //lo hace el boton
        nextConversation = true;
    }

    public void SetNext(bool b)
    {
        next = b;
    }

    private IEnumerator ShowDialog(RTS_ICoversation conversation)
    {
        RTS_Dialog dialog = (RTS_Dialog)conversation;
        scenario.sprite = dialog.scenario;

        for (int i = 0; i < dialog.sentences.Count; i++)
        {
            showingText.text = "";
            int j = 0;
            overtake = next = false;
            actorImage.sprite = dialog.sentences[i].actor.expreccions[(int)dialog.sentences[i].expression];
            actorName.text = dialog.sentences[i].actor.name;

            //mientras se escribe la oracion
            while (showingText.text.Length < dialog.sentences[i].text.Length)
            {
                if (overtake)
                {
                    showingText.text = dialog.sentences[i].text;
                    break;
                }
                yield return new WaitForSeconds(0.1f);
                showingText.text += dialog.sentences[i].text[j];
                j++;
            }

            overtake = true;
            yield return new WaitUntil(() => next);

        }
        yield return new WaitUntil(() => next);
        actual = dialog.next;
        nextConversation = true;
    }

    private void Choose(RTS_ICoversation conversation)
    {
        RTS_Choose choose = (RTS_Choose)conversation;
        choose.condition.value = choose.changeTo;
        actual = choose.next;
        nextConversation = true;
    }

    private void Condition(RTS_ICoversation conversation)
    {
        Condition condition = (Condition)conversation;
        if(condition.IsTrue())
        {
            actual = condition.nextAreTrue;
        }
        else
        {
            actual = condition.nextAreFalse;
        }
        nextConversation = true;
    }

}
