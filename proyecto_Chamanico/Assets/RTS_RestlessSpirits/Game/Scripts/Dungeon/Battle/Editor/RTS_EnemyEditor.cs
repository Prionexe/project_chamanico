﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[UnityEditor.CustomEditor(typeof(RTS_Enemy))]
public class RTS_EnemyEditor : Editor
{
    public RTS_Enemy enemy;

    void OnEnable()
    {
        enemy = (RTS_Enemy)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        // header
        /*
        GUILayout.Label("Enemy Data", EditorStyles.boldLabel);

        base.OnInspectorGUI();
        
        foreach (RTS_Skill s in enemy.skills)
        {
            GUILayout.Label("Skill:", EditorStyles.boldLabel);
            GUILayout.Space(6);
            GUILayout.Label("Action:");
            s.action = EditorGUILayout.TextArea(s.action);
            GUILayout.Label("Button Text:");
            s.text = EditorGUILayout.TextArea(s.text);
            GUILayout.Label("Description:");
            s.description = EditorGUILayout.TextArea(s.description);
            GUILayout.Label("Damage:");
            s.dmgEffect = EditorGUILayout.IntField(s.dmgEffect);
            GUILayout.Label("Heal:");
            s.healEffect = EditorGUILayout.IntField(s.healEffect);
            GUILayout.Label("Delay:");
            s.wait = EditorGUILayout.FloatField(s.wait);

           
            if (GUILayout.Button("remove", GUILayout.ExpandWidth(false)))
            {
                enemy.skills.Remove(s);
            }
         
            GUILayout.Space(12);
        }

        if (GUILayout.Button("Add Skill", GUILayout.Height(40)))
        {
            enemy.skills.Add(new RTS_Skill());
        }


        if (GUI.changed)
        {
            EditorUtility.SetDirty(enemy);
        }*/
    }
}
