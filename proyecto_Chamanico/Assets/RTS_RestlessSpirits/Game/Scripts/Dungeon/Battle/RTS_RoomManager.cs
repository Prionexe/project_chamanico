﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RTS_RoomManager : MonoBehaviour
{
    //PLAYER
    public RTS_Player player;
    public Image playerLifeBar;
    public Image playerManaBar;
    public Image playerSprite;
    public Text playerArmor;
    public Text playerInt;
    public Text playerStr;
    public List<RTS_Skill> playerAvaibleActions;
    public Text[] playerActionTexts;
    public GameObject buttonsPanel;

    //ROOM
    public RTS_Room_V2 room;

    //ENEMIE
    public RTS_Enemy enemie;
    public Image enemieLifeBar;
    public Image enemieManaBar;
    public Image enemieSprite;
    public Text enemieArmor;
    public Text enemieInt;
    public Text enemieStr;

    public bool turnDone;
    public bool skillDone;
    public bool playerTurn;
    public int turn;

    public Text battleText;
    public Color fullLife;
    public Color halfLife;
    public Color emptyLife;

    public Image background;

    public AudioSource sfx;

    public Animator playerAnimator;
    public Animator enemieAnimator;

    // Use this for initialization
    void Start ()
    {
        background.sprite = RTS_GameManager.Instance.actualDungeon.GetRoomBackground();
        buttonsPanel.SetActive(false);
        playerTurn = true;
        turnDone = false;
        skillDone = false;

        player = RTS_GameManager.Instance.player;
        playerSprite.sprite = player.sprite;
        SetPlayerActions();
        player.InitCharacter();

        InitEnemie();
        enemieSprite.sprite = enemie.sprite;
        battleText.text = enemie.presentationText;
        enemie.InitCharacter();
        ActualizeLifes(0.5f);
        StartCoroutine(this.Battle());
    }
    
    public void InitEnemie()
    {
        RTS_Dungeon d = RTS_GameManager.Instance.actualDungeon;
        Debug.Log(d);
        RTS_Room_V2 r = d.selectedRoom;
        Debug.Log(r);
        RTS_Enemy aux = r.GetEnemie();
        Debug.Log(aux);
        enemie = new RTS_Enemy
        {
            maxLife = aux.maxLife,
            life = aux.life,
            maxMana = aux.maxMana,
            mana = aux.mana,
            baseInt = aux.baseInt,
            intelligence = aux.intelligence,
            baseStr = aux.baseStr,
            strength = aux.strength,
            baseArmor = aux.baseArmor,
            armor = aux.armor,
            skillbundle = aux.skillbundle,
            bonusArmor = aux.bonusArmor,
            bonusInt = aux.bonusInt,
            bonusLife = aux.bonusLife,
            bonusMana = aux.bonusMana,
            bonusStr = aux.bonusStr,
            skills = aux.skills,
            presentationText = aux.presentationText,
            sprite = aux.sprite,
            dyingFrase = aux.dyingFrase,
            type = aux.type,
            victoryFrase = aux.victoryFrase,
            dungeonOptionText = aux.presentationText
        };
    }
    
    private void SetPlayerActions()
    {
        List<RTS_Skill> skills = new List<RTS_Skill>();
        while(skills.Count < 4)
        {
            RTS_Skill s = player.skills[Random.Range(0,player.skills.Count-1)];
            if(!(skills.Contains(s)))
            {
                skills.Add(s);
            }
        }
        playerAvaibleActions = skills;
        for (int i = 0; i < 4; i++)
        {
            playerActionTexts[i].text = playerAvaibleActions[i].text + "\n" + playerAvaibleActions[i].action;
        }
        buttonsPanel.SetActive(true);
    }

    public IEnumerator DoSkill(RTS_Character caster, RTS_Character objective, RTS_Skill skill)
    {
        StartCoroutine(WriteBattleText(skill.GetDescription(caster)));
        skill.DoSkill(caster, objective, sfx, enemieAnimator, playerAnimator);
        yield return new WaitForSeconds(0.15f + skill.description.Length/RTS_GameManager.Instance.gameOptions.textSpeed);
        StartCoroutine(ActualizeStats(skill.wait));
        yield return new WaitForSeconds(skill.wait + 0.3f);
        turnDone = true;
    }

    public void PlayerSkill(int i)
    {
        if(playerTurn && skillDone == false)
        StartCoroutine(DoSkill(player, enemie, playerAvaibleActions[i]));
        skillDone = true;
    }

    //BATTLE
    IEnumerator Battle()
    {
        bool cleared = false;
        while (player.life > 0 && enemie.life > 0)
        {
            turn++;
            turnDone = false;
            if(playerTurn)
            {
                skillDone = false;
                if(player.life > 0 && enemie.life > 0) SetPlayerActions();
                yield return new WaitUntil(() => turnDone);
                buttonsPanel.SetActive(false);
            }
            else
            {
                int i = Random.Range(0, enemie.skills.Count);
                if (player.life > 0 && enemie.life > 0) StartCoroutine(DoSkill(enemie, player, enemie.skills[i]));
                yield return new WaitUntil(() => turnDone);
            }
            playerTurn = !playerTurn;
            yield return new WaitForSeconds(1);
            if (!(player.life > 0 && enemie.life > 0)) cleared = true;
        }
        yield return new WaitUntil(() => cleared);
        if (enemie.life <= 0)
        {
            Debug.Log(enemie.dyingFrase);
            StartCoroutine(WriteBattleText(enemie.dyingFrase));
            yield return new WaitForSeconds(1);
            StartCoroutine(WriteBattleText(enemie.OnDead(player)));
        }
        else
        {
            StartCoroutine(WriteBattleText(enemie.victoryFrase));
            player.life = player.maxLife;
            RTS_GameManager.Instance.LoadScene("RTS_YouLose");
        }
        yield return new WaitForSeconds(1);
        Debug.Log(" 1 : " + enemie.type);
        RTS_GameManager.Instance.actualDungeon.NextRoom(enemie.type);
    }

    IEnumerator ActualizeStats(float time)
    {
        Debug.Log("Actualize Stats");
        ActualizeManas(time/2);
        yield return new WaitForSeconds(time + 0.1f);
        ActualizeLifes(time);
        yield return new WaitForSeconds(time + 0.1f);
        playerArmor.text = player.armor.ToString();
        playerInt.text = player.intelligence.ToString();
        playerStr.text = player.strength.ToString();
        enemieArmor.text = enemie.armor.ToString();
        enemieInt.text = enemie.intelligence.ToString();
        enemieStr.text = enemie.strength.ToString();
    }

    public void ActualizeLifes(float time)
    {
        Debug.Log("Life");
        float pLife = (float)player.life / player.maxLife;
        float eLife = (float)enemie.life / enemie.maxLife;
        if (playerLifeBar.fillAmount != pLife) StartCoroutine(ActualizeLifeBar(playerLifeBar, pLife, time));
        if (enemieLifeBar.fillAmount != eLife) StartCoroutine(ActualizeLifeBar(enemieLifeBar, eLife, time));
        if(turn == 0)
        {
            StartCoroutine(ActualizeLifeBar(playerLifeBar, pLife, 0.01f));
            StartCoroutine(ActualizeLifeBar(enemieLifeBar, eLife, 0.01f));
        }
    }

    IEnumerator ActualizeLifeBar(Image lifeBar, float fillAmount, float time)
    {
        Debug.Log("LifeBars");
        Color c = Color.white;
        if(fillAmount >= 0.5)
        {
            c.r = fullLife.r + (Mathf.Abs(halfLife.r - fullLife.r) * 2 * (1 - fillAmount));
            c.g = fullLife.g + (Mathf.Abs(halfLife.g - fullLife.g) * 2 * (1 - fillAmount));
            c.b = fullLife.b + (Mathf.Abs(halfLife.b - fullLife.b) * 2 * (1 - fillAmount));
            c.a = fullLife.a + (Mathf.Abs(halfLife.a - fullLife.a) * 2 * (1 - fillAmount));
        }
        else
        {
            c.r = emptyLife.r + (Mathf.Abs(halfLife.r - emptyLife.r) * 2 * fillAmount);
            c.g = emptyLife.g + (Mathf.Abs(halfLife.g - emptyLife.g) * 2 * fillAmount);
            c.b = emptyLife.b + (Mathf.Abs(halfLife.b - emptyLife.b) * 2 * fillAmount);
            c.a = emptyLife.a + (Mathf.Abs(halfLife.a - emptyLife.a) * 2 * fillAmount);
        }
        while (time > 0)
        {
            lifeBar.fillAmount = Mathf.Lerp(lifeBar.fillAmount, fillAmount, Time.deltaTime/(lifeBar.fillAmount - fillAmount));
            lifeBar.color = Color.Lerp(lifeBar.color, c, Time.deltaTime / (lifeBar.fillAmount - fillAmount));
            time -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        lifeBar.fillAmount = fillAmount;
        lifeBar.color = c;
    }

    public void ActualizeManas(float time)
    {
        Debug.Log("Mana");
        float pMana = (float)player.mana / player.maxMana;
        Debug.Log(player.mana);
        float eMana = (float)enemie.mana / enemie.maxMana;
        Debug.Log(enemie.mana);
        if (playerManaBar.fillAmount != pMana) StartCoroutine(ActualizeManaBar(playerManaBar, pMana, time));
        if (enemieManaBar.fillAmount != eMana) StartCoroutine(ActualizeManaBar(enemieManaBar, eMana, time));
        if (turn == 0)
        {
            StartCoroutine(ActualizeManaBar(playerManaBar, pMana, 0.01f));
            StartCoroutine(ActualizeManaBar(enemieManaBar, eMana, 0.01f));
        }
    }

    IEnumerator ActualizeManaBar(Image manaBar, float fillAmount, float time)
    {
        Debug.Log("ManaBar");
        while (time > 0)
        {
            manaBar.fillAmount = Mathf.Lerp(manaBar.fillAmount, fillAmount, Time.deltaTime / (manaBar.fillAmount - fillAmount));
            time -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        manaBar.fillAmount = fillAmount;
    }

    IEnumerator WriteBattleText(string txt)
    {
        if (playerTurn) battleText.text = "Shad : ";
        else if (enemie.type == CharacterType.NEUTRAL) battleText.text = "Unknown : ";
        else battleText.text = "Enemy : ";
        if (turn == 0) battleText.text = "";
        for(int i = 0; i < txt.Length; i++)
        {
            battleText.text += txt[i];
            yield return new WaitForSeconds(1/RTS_GameManager.Instance.gameOptions.textSpeed);
        }
    }

    public void CloseGame()
    {
        RTS_GameManager.Instance.CloseGame();
    }
}
