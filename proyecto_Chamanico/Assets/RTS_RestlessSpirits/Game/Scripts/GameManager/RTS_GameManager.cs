﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RTS_GameManager
{
    private static RTS_GameManager _Instance;
    public static RTS_GameManager Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = new RTS_GameManager();
            }
            return _Instance;
        }
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("RTS_MainMenu");
    }


    public List<RTS_Dungeon> dungeons;
    public RTS_Dungeon actualDungeon;

    public RTS_Player player;
    public RTS_PlayerData playerData;

    public RTS_GameOptions gameOptions;
    public RTS_HistoryFlow historyFlow;

    public void SetDialog(int i)
    {
        historyFlow.SetActual(stage,i);
    }
    
    public int stage = 0;
    public int person = 0;
    

    public void NewPlayer(string name)
    {
        playerData.playerName = name;
    }

    public void NewGame()
    {
        SceneManager.LoadScene("RTS_Dialog");
    }

    public void EnterDungeon(int i)
    {
        if(_Instance.dungeons[i].cleared == false)
        {
            SetDungeon(i);
            SceneManager.LoadScene("RTS_Room");
        }
    }

    public void SetDungeon(int i)
    {
        _Instance.player.life = _Instance.player.maxLife;
        _Instance.actualDungeon = _Instance.dungeons[i];
        _Instance.actualDungeon.lvl = 0;
        _Instance.actualDungeon.ActualizeChances();
        _Instance.actualDungeon.selectedRoom = _Instance.actualDungeon.GetRoom();
    }

    public void MenuScene()
    {
        SceneManager.LoadScene("RTS_MainMenu");
    }

    public void Room()
    {
        SceneManager.LoadScene("RTS_Room");
    }

    public void Hallway()
    {
        actualDungeon.lvl++;
        actualDungeon.ActualizeRoomOptions();
        SceneManager.LoadScene("RTS_Hallway");
    }

    public void Dialog()
    {
        SceneManager.LoadScene("RTS_Dialog");
    }

    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
