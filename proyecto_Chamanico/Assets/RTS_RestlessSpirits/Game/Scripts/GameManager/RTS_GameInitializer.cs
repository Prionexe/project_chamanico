﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTS_GameInitializer : MonoBehaviour
{
    public List<RTS_Actor> actros;
    public List<RTS_Dungeon> dungeons;
    public RTS_Player player;
    public RTS_PlayerSkills playerSkills;
    public RTS_PlayerData playerData;
    public RTS_GameOptions gameOptions;
    public RTS_HistoryFlow historyFlowManager;
    public RTS_Player baseplayer;

	// Use this for initialization
	void Awake ()
    {
        player.maxLife = baseplayer.maxLife;
        player.life = baseplayer.life;
        player.baseInt = baseplayer.baseInt;
        player.intelligence = baseplayer.intelligence;
        player.baseStr = baseplayer.baseStr;
        player.strength = baseplayer.strength;
        player.baseArmor = baseplayer.baseArmor;
        player.armor = baseplayer.armor;
        player.maxMana = baseplayer.maxMana;
        player.mana = baseplayer.mana;
        player.skills = baseplayer.skills;

        RTS_GameManager.Instance.dungeons = dungeons;
        //RTS_GameManager.Instance.actualDungeon = dungeons[0];
        RTS_GameManager.Instance.player = player;
        RTS_GameManager.Instance.player.skills = playerSkills.skills;
        RTS_GameManager.Instance.playerData = playerData;
        RTS_GameManager.Instance.gameOptions = gameOptions;
        historyFlowManager.actual = historyFlowManager.acts[0].dialogs[0];
        RTS_GameManager.Instance.historyFlow = historyFlowManager;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
