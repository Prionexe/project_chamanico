﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RTS_GameOptions : ScriptableObject
{
    public float musicVol;
    public float soundVol;
    public float textSpeed;

    /*
    public static RTS_GameOptions Create()
    {
        RTS_GameOptions GameOptions = ScriptableObject.CreateInstance<RTS_GameOptions>();

        AssetDatabase.CreateAsset(GameOptions, "Assets/RTS_RestlessSpirits/GameOptions/GameOptions.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = GameOptions;
        return GameOptions;
    }
    */
}
