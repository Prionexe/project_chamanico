﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Fight System/Skills Bundle")]
public class RTS_PlayerSkills : ScriptableObject
{
    public List<RTS_Skill> skills = new List<RTS_Skill>();
}
