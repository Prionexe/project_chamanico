﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Fight System/Dungeon")]
public class RTS_Dungeon : ScriptableObject
{
    // nico
    public List<RTS_Room_V2> BasicRooms;
    public List<RTS_Room_V2> EliteRooms;
    public List<RTS_Room_V2> BossRooms;

    //gabo
    public bool cleared;
    public int depth;
    public int lvl;

    public string scene;
    public int dialog;

    public List<Sprite> roomBackgrounds;
    public List<Sprite> hallwayBackgrounds;
    public List<string> hallwayText;

    public float basicEnemieChances;
    public float eliteEnemieChances;
    public float neutralEnemieChances;

    public List<RTS_Room_V2> nextRoomOptions;
    public RTS_Room_V2 selectedRoom;

    public RTS_Room_V2 GetRoom()
    {
        float r = Random.Range(0.1f, 100.0f);
        if (lvl >= depth - 1)
        {
            Debug.Log("here : " + BossRooms.Count);
            RTS_Room_V2 b = BossRooms[Random.Range(0, BossRooms.Count)];
            Debug.Log(b);
            return b;
        }
        if (r <= basicEnemieChances)
        {
            return BasicRooms[Random.Range(0, BasicRooms.Count)];
        }
        r = r - basicEnemieChances;
        if (r <= eliteEnemieChances)
        {
            return EliteRooms[Random.Range(0, EliteRooms.Count)];
        }
        return BasicRooms[Random.Range(0, BasicRooms.Count)];
    }

    public void NextRoom(CharacterType r)
    {
        lvl++;
        ActualizeRoomOptions();
        Debug.Log(r);
        if (r == CharacterType.BOSS)
        {
            ClearDungeon();
        }
        else
        {
            RTS_GameManager.Instance.LoadScene("RTS_Hallway");
        }
    }

    public void ActualizeChances()
    {
        basicEnemieChances = 100 - (lvl*100/depth);
        eliteEnemieChances = lvl * 100 / depth;
        neutralEnemieChances = lvl * 100 / depth;
    }

    public void ActualizeRoomOptions()
    {
        ActualizeChances();
        int r = Random.Range(0,4); // have to test
        for(int i = 0; i < nextRoomOptions.Count; i++)
        {
            if (i > r) nextRoomOptions[i] = null;
            nextRoomOptions[i] = GetRoom();
        }
    }

    public void SetNextRoom(int i)
    {
        selectedRoom = nextRoomOptions[i];
    }

    public Sprite GetRoomBackground()
    {
        return roomBackgrounds[Random.Range(0, roomBackgrounds.Count)];
    }

    public Sprite GetHallwayBackground()
    {
        return hallwayBackgrounds[Random.Range(0, hallwayBackgrounds.Count)];
    }

    public string GetHallwayText()
    {
        int i = Random.Range(0, hallwayText.Count);
        return hallwayText[i];
    }

    public void ClearDungeon()
    {
        this.cleared = true;
        RTS_GameManager.Instance.SetDialog(dialog);
        RTS_GameManager.Instance.LoadScene(scene);
    }

}
