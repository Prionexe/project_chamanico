﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Fight System/Enemy")]
[System.Serializable]
public class RTS_Enemy : RTS_Character
{
    public string presentationText;
    public string dungeonOptionText;
    public string dyingFrase;
    public string victoryFrase;

    public int bonusInt;
    public int bonusStr;
    public int bonusMana;
    public int bonusLife;
    public int bonusArmor;

    public RTS_PlayerSkills skillbundle;

    public string OnDead(RTS_Player player)
    {
        string phrase = "You earned : ";
        player.maxLife += bonusLife;
        player.life += bonusLife;
        if (bonusLife != 0) phrase += "Life + " + bonusMana + "\n";
        player.maxMana += bonusMana;
        player.mana += bonusMana;
        if (bonusMana != 0) phrase += "Mana + " + bonusMana + "\n";
        player.baseInt += bonusInt;
        if (bonusInt != 0) phrase += "Intelligence + " + bonusMana + "\n";
        player.baseStr += bonusStr;
        if (bonusStr != 0) phrase += "Strength + " + bonusMana + "\n";
        player.baseArmor = bonusArmor;
        if (bonusArmor != 0) phrase += "Armor + " + bonusMana + "\n";
        if (skillbundle != null)
        {
            phrase +="Skills learned : \n";
            foreach (RTS_Skill s in skillbundle.skills)
            {
                player.skills.Add(s);
                phrase += s.text + "\n";
            }
        }

        return phrase;
    }
}
