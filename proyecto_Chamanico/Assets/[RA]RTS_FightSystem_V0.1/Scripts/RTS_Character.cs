﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RTS_Character : ScriptableObject
{
    public int maxLife;
    public int life;
    public int baseInt;
    public int intelligence;
    public int baseStr;
    public int strength;
    public int baseArmor;
    public int armor;
    public int mana;
    public int maxMana;
    public Sprite sprite;
    public CharacterType type;
    public List<RTS_Skill> skills = new List<RTS_Skill>();

    public void InitCharacter()
    {
        mana = maxMana;
        intelligence = baseInt;
        strength = baseStr;
        armor = baseArmor;
    }
    
    public void SetLife(int l)
    {
        if (l > maxLife) this.life = maxLife;
        else if (l < 0) this.life = 0;
        else this.life = l;
    }

    public void ActualizeLife(int delta)
    {
        life += delta;
        SetLife(life);
    }

    public void SetMana(int m)
    {
        if (m > maxMana) this.mana = maxMana;
        else if (m < 0) this.life = 0;
        else this.mana = m;
    }

    public void ActualizeMana(int delta)
    {
        mana += delta;
        SetMana(mana);
    }
}

[System.Serializable]
public enum CharacterType
{
    ELITE_ENEMY,
    NORMAL_ENEMY,
    NEUTRAL,
    PLAYER,
    BOSS,
}
