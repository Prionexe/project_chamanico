﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// room deberia ser una superclase para enemyroom, bossroom y hallway

[System.Serializable]
[CreateAssetMenu(menuName = "Fight System/Room")]
public class RTS_Room_V2 : ScriptableObject
{
    public RoomType type;
    public List<RTS_Enemy> BaseEnemies;

    public RTS_Enemy GetEnemie()
    {
        RTS_Enemy e = BaseEnemies[Random.Range(0, BaseEnemies.Count)];
        Debug.Log(e);
        return e;
    }

}

[System.Serializable]
public enum RoomType
{
    BASIC,
    ELITE,
    BOSS,
    HALLWAY 
}
