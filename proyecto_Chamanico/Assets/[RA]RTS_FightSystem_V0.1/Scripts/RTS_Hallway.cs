﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RTS_Hallway : MonoBehaviour
{
    public Text hallwayText;
    public List<Button> options;
    public string noRoadText;
    public bool done;
    public Image background;

	// Use this for initialization
	void Start ()
    {
        background.sprite = RTS_GameManager.Instance.actualDungeon.GetHallwayBackground();
        hallwayText.text = "";
        foreach(Button t in options)
        {
            t.GetComponentInChildren<Text>().text = "";
            t.gameObject.SetActive(false);
        }
        StartCoroutine(Hallway());
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    IEnumerator Hallway()
    {
        StartCoroutine(WriteHallwayText(RTS_GameManager.Instance.actualDungeon.GetHallwayText(),hallwayText));
        yield return new WaitUntil(() => done);
        SetPanelOPtions();
    }

    public void SetPanelOPtions()
    {
        for(int i = 0; i < RTS_GameManager.Instance.actualDungeon.nextRoomOptions.Count; i++)
        {
            options[i].gameObject.SetActive(true);
            StartCoroutine(WriteHallwayText(RTS_GameManager.Instance.actualDungeon.nextRoomOptions[i].GetEnemie().dungeonOptionText, options[i].GetComponentInChildren<Text>()));
        }
    }

    IEnumerator WriteHallwayText(string txt, Text text)
    {
        text.text = "";
        for (int i = 0; i < txt.Length; i++)
        {
            text.text += txt[i];
            yield return new WaitForSeconds(1 / RTS_GameManager.Instance.gameOptions.textSpeed);
        }
        done = true;
    }

    public void SelectPath(int i)
    {
        RTS_GameManager.Instance.actualDungeon.SetNextRoom(i);
        RTS_GameManager.Instance.Room();
    }
}
