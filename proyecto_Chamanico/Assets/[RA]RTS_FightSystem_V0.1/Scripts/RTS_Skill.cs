﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class RTS_Skill
{ 
    public string text;
    public string action;
    public string description;

    public int healEffect;
    public int manaEffect;
    public int armorEffect;
    public int intEffect;
    public int strEffect;

    public int dmgEffect;
    public int purifyEffect;
    public int crackEffect;
    public int demoralizeEffect;
    public int weakenEffect;

    public AudioClip sound;

    public float wait;

    public void DoSkill(RTS_Character caster, RTS_Character objective, AudioSource sfx, Animator enemie, Animator player)
    {
        if (caster.mana + manaEffect >= 0)
        {
            sfx.PlayOneShot(sound);
            if (healEffect > 0)
            {
                if (caster.type == CharacterType.PLAYER)
                    player.Play("heal");
                else
                    enemie.Play("heal_e");
            }
            caster.life += healEffect + (healEffect * caster.intelligence / 10);
            caster.mana += manaEffect + (manaEffect * caster.intelligence / 10);
            if ((dmgEffect + (dmgEffect * caster.strength / 10) - objective.armor) > 0)
            {
                if (caster.type == CharacterType.PLAYER)
                {
                    enemie.Play("hurt_e");
                    player.Play("attack");
                }
                else
                {
                    player.Play("hurt");
                    enemie.Play("attack_e");
                }
                objective.ActualizeLife(-(dmgEffect + (dmgEffect * caster.strength / 10) - objective.armor));

            }
            if ((purifyEffect + (purifyEffect * caster.intelligence / 10) > 0))
                objective.ActualizeMana(-(purifyEffect + (purifyEffect * caster.intelligence / 10)));
            if(armorEffect > 0 || intEffect >0 || strEffect > 0)
            {
                if (caster.type == CharacterType.PLAYER)
                    player.Play("buffo_inteligence");
                else
                    enemie.Play("buffo_e");
            }
            caster.armor += armorEffect;
            caster.intelligence += intEffect;
            caster.strength += strEffect;
            if(crackEffect > 0 || demoralizeEffect > 0 || weakenEffect > 0)
            {
                if (caster.type == CharacterType.PLAYER)
                    enemie.Play("debuff_e");
                else
                    player.Play("debuff_inteligence");
            }
            objective.armor -= crackEffect;
            objective.intelligence -= demoralizeEffect;
            objective.strength -= weakenEffect;
        }
    }
    
    public string GetDescription(RTS_Character caster)
    {
        Debug.Log((caster.mana + manaEffect) + "  " + caster.mana + "  " + manaEffect);
        if (caster.mana + manaEffect >= 0)
        {
            return description;
        }
        return ("Tries to " + description + ", but dosn't have enough mana");
    }
}
